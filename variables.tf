variable "hcloud_token" {}

variable "name" { 
  default = "gramola" 
}

variable "image" { 
  type = string
  default = "ubuntu-20.04"
}

variable "server_type" { 
  type = string
  default = "cx11"
}

variable "location" { 
  type = string
  default = "hel1" 
}

variable "username" {
  type = string
  default = "gramola"
}
