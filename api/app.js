const express = require('express');
const logger = require('./utils/logger');
const app = express();

app.use(express.json())

app.post('/slack/hello', (req, res) => {
  res.send('hello world');
  logger.info(req.body);
});

module.exports = app 
