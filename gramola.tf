
provider "hcloud" {                                                 # Provider designation hetzner
 token = var.hcloud_token                                           # The token for connection to the hetzner API is specified in the terraform.tfvars file
}

resource "hcloud_server" "ubuntu" {                                 # Create a server
  name = "server-${var.name}-prod"                                       # Name server
  image = var.image                                                 # Basic image
  server_type = var.server_type                                     # Instance type
  location = var.location                                           # Region
  backups = "false"                                                 # Enable backups
  ssh_keys = ["${hcloud_ssh_key.gkey.id}"]                          # SSH key
  user_data = data.template_file.init.rendered
  
  provisioner "file" {                                       
    source = "config/.zshrc"
    destination = "/root/.zshrc"
  }

  connection {
    type = "ssh"
    host = self.ipv4_address
    timeout = "1m"
    private_key = local.private_key
  }
}

data template_file "init" {
   template = file("${path.module}/config/init.yml")

   vars = {
     server_name = var.name
     username = var.username
     public_key = local.public_key
   }
}

# Definition ssh key from variable
resource "hcloud_ssh_key" "gkey" {
  name = "gkey"
  public_key = local.public_key
}

locals {
  public_key = file("~/.ssh/${var.username}.pub")
  private_key = file("~/.ssh/${var.name}")
}
